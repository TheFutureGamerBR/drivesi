-- *********************************************
-- * Standard SQL generation                   *
-- *-------------------------------------------*
-- * Generator date:  Feb 26 2010              *
-- * Generation date: Mon Apr 05 15:41:25 2010 *
-- ********************************************* 


-- Database Section
-- ________________ 

create database Tutorial Relational Model;


-- DBSpace Section
-- _______________

create dbspace Bibliography Collection;


-- Tables Section
-- _____________ 

create table Authors (
     Id_author char(1) not null,
     Name char(1) not null,
     Add_Street -- Compound attribute -- not null,
     Add_City char(1) not null,
     email char(1),
     constraint ID_Authors_ID primary key (Id_author));

create table Authorship_book (
     Id_author char(1) not null,
     Id_bib numeric(10) not null,
     constraint ID_Authorship_book_ID primary key (Id_bib, Id_author));

create table Authorship_paper (
     Id_author char(1) not null,
     Id_paper char(1) not null,
     constraint ID_Authorship_paper_ID primary key (Id_author, Id_paper));

create table Bibliography_Item (
     Id_bib numeric(10) not null,
     Name char(1) not null,
     Periodical numeric(10),
     Book numeric(10),
     constraint ID_Bibliography_Item_ID primary key (Id_bib));

create table Book (
     Id_bib numeric(10) not null,
     Name -- Compound attribute -- not null,
     Ano char(1) not null,
     ISBN char(1) not null,
     constraint FKBib_Boo_ID primary key (Id_bib));

create table Borrowing (
     Id_bib numeric(10) not null,
     RG -- Compound attribute -- not null,
     CPF char(1) not null,
     constraint FKBor_Bib_ID primary key (Id_bib));

create table Client (
     CPF char(1) not null,
     RG -- Compound attribute -- not null,
     Name char(1) not null,
     constraint ID_Client_ID primary key (RG -- Compound attribute --, CPF));

create table Papers (
     Id_paper char(1) not null,
     Title -- Compound attribute -- not null,
     Pages char(1) not null,
     Id_bib numeric(10) not null,
     constraint ID_Papers_ID primary key (Id_paper));

create table Periodical (
     Id_bib numeric(10) not null,
     Name char(1) not null,
     Volume char(1) not null,
     ISSN char(1) not null,
     constraint FKBib_Per_ID primary key (Id_bib));

create table Samples (
     Id_bib numeric(10) not null,
     Samples varchar(1) not null,
     constraint ID_Samples_ID primary key (Id_bib, Samples));


-- Constraints Section
-- ___________________ 

alter table Authorship_book add constraint FKAut_Boo
     foreign key (Id_bib)
     references Book;

alter table Authorship_book add constraint FKAut_Aut_1_FK
     foreign key (Id_author)
     references Authors;

alter table Authorship_paper add constraint FKAut_Pap_FK
     foreign key (Id_paper)
     references Papers;

alter table Authorship_paper add constraint FKAut_Aut
     foreign key (Id_author)
     references Authors;

alter table Bibliography_Item add constraint ID_Bibliography_Item_CHK
     check(exists(select * from Samples
                  where Samples.Id_bib = Id_bib)); 

alter table Bibliography_Item add constraint EXTONE_Bibliography_Item
     check((Book is not null and Periodical is null)
           or (Book is null and Periodical is not null)); 

alter table Book add constraint FKBib_Boo_CHK
     check(exists(select * from Authorship_book
                  where Authorship_book.Id_bib = Id_bib)); 

alter table Book add constraint FKBib_Boo_FK
     foreign key (Id_bib)
     references Bibliography_Item;

alter table Borrowing add constraint FKBor_Cli_FK
     foreign key (RG -- Compound attribute --, CPF)
     references Client;

alter table Borrowing add constraint FKBor_Bib_FK
     foreign key (Id_bib)
     references Bibliography_Item;

alter table Papers add constraint ID_Papers_CHK
     check(exists(select * from Authorship_paper
                  where Authorship_paper.Id_paper = Id_paper)); 

alter table Papers add constraint FKSet_of_FK
     foreign key (Id_bib)
     references Periodical;

alter table Periodical add constraint FKBib_Per_CHK
     check(exists(select * from Papers
                  where Papers.Id_bib = Id_bib)); 

alter table Periodical add constraint FKBib_Per_FK
     foreign key (Id_bib)
     references Bibliography_Item;

alter table Samples add constraint FKBib_Sam
     foreign key (Id_bib)
     references Bibliography_Item;


-- Index Section
-- _____________ 

create unique index ID_Authors_IND
     on Authors (Id_author);

create unique index ID_Authorship_book_IND
     on Authorship_book (Id_bib, Id_author);

create index FKAut_Aut_1_IND
     on Authorship_book (Id_author);

create unique index ID_Authorship_paper_IND
     on Authorship_paper (Id_author, Id_paper);

create index FKAut_Pap_IND
     on Authorship_paper (Id_paper);

create unique index ID_Bibliography_Item_IND
     on Bibliography_Item (Id_bib);

create unique index FKBib_Boo_IND
     on Book (Id_bib);

create index FKBor_Cli_IND
     on Borrowing (RG -- Compound attribute --, CPF);

create unique index FKBor_Bib_IND
     on Borrowing (Id_bib);

create unique index ID_Client_IND
     on Client (RG -- Compound attribute --, CPF);

create unique index ID_Papers_IND
     on Papers (Id_paper);

create index FKSet_of_IND
     on Papers (Id_bib);

create unique index FKBib_Per_IND
     on Periodical (Id_bib);

create unique index ID_Samples_IND
     on Samples (Id_bib, Samples);

